// vendor
import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'reset-css';

// proj

import store, { history } from './store';
import Routes from './routes/Routes';

class App extends Component {
    render() {
        return (
            <Provider store={ store }>
                {/* <PersistGate loading={ null } persistor={ persistor }> */}
                    <Router history={ history }>
                        <Routes />
                    </Router>
                {/* </PersistGate> */}
            </Provider>
        );
    }
}

export default App;
