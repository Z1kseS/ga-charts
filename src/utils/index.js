// utils entry point
import { fetchAPI } from './fetchAPI.js';
export * from './sideEffects.js';
export * from './functions';

export { fetchAPI };
