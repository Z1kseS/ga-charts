//
// localStorage
//

// id
export const setId = id => localStorage.setItem('__ga_id__', id);

export const getId = () => localStorage.getItem('__ga_id__');

export const removeId = () => localStorage.removeItem('__ga_id__');
