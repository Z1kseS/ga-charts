export const functions = {
    deba1: {
        evaluate:  x => Math.pow(Math.sin(5 * Math.PI * x), 6),
        min:       0,
        max:       1,
        precision: 1000,
    },
    deba2: {
        evaluate: x => {
            const first = Math.pow(
                Math.E,
                -2 * Math.log(2) * Math.pow((x - 0.1) / 0.8, 2),
            );
            const second = Math.pow(Math.sin(5 * Math.PI * x), 6);

            return first * second;
        },
        min:       0,
        max:       1,
        precision: 1000,
    },
    deba3: {
        evaluate: x => {
            return Math.pow(
                Math.sin(5 * Math.PI * (Math.pow(x, 0.75) - 0.05)),
                6,
            );
        },
        min:       0,
        max:       1,
        precision: 1000,
    },
    deba4: {
        evaluate: x => {
            const first = Math.pow(
                Math.E,
                -2 * Math.log(2) * Math.pow((x - 0.08) / 0.854, 2),
            );
            const second = Math.pow(
                Math.sin(5 * Math.PI * (Math.pow(x, 0.75) - 0.05)),
                6,
            );

            return first * second;
        },
        min:       0,
        max:       1,
        precision: 1000,
    },
    d6: {
        evaluate: x => {
            if (x >= 0 && x < 2.5) {
                return 80 * (2.5 - x);
            } else if (x >= 2.5 && x < 5) {
                return 64 * (x - 2.5);
            } else if (x >= 5 && x < 7.5) {
                return 64 * (7.5 - x);
            } else if (x >= 7.5 && x < 12.5) {
                return 28 * (x - 7.5);
            } else if (x >= 12.5 && x < 17.5) {
                return 28 * (17.5 - x);
            } else if (x >= 17.5 && x < 22.5) {
                return 32 * (x - 17.5);
            } else if (x >= 22.5 && x < 27.5) {
                return 32 * (27.5 - x);
            } else if (x <= 30) {
                return 80 * (x - 27.5);
            }

            throw new Error();
        },
        min:       0,
        max:       30,
        precision: 100,
    },
    f31: {
        evaluate: x => {
            const values = [x];
            let result1 = 0;
            let result2 = 0;

            for (let i = 0; i < values.length; i++) {
                result1 += Math.abs(values[i]);
                result2 += Math.pow(values[i], 2);
            }
            return result1 * Math.exp(-result2);
        },
        min:       -10,
        max:       10,
        precision: 100,
    },
    f20: {
        evaluate: x => {
            let firstArgument = 0;
            const values = [x];

            for (let value of values) {
                firstArgument += 10 * Math.cos(2 * Math.PI * value) - Math.pow(value, 2);
            }

            //System.out.println(firstArgument - 10 * values.length);
            return firstArgument - 10 * values.length;
        },
        min:       -5.12,
        max:       5.12,
        precision: 1000,
    },
    f22: {
        evaluate: x => {
            const values = [x];
            let firstArgument = values.length;
            let secondArgument = 0;
            let thirdArgument = 1;

            for (let value of values) {
                secondArgument += Math.pow(value, 2) / 4000;
            }

            for (let i = 0; i < values.length; i++) {
                thirdArgument *= Math.cos(values[i] / Math.pow(i + 1, 0.5));
            }

            return firstArgument - (secondArgument - thirdArgument + 1);
        },
        min:       -600,
        max:       600,
        precision: 10,
    }
};
