// vendor
import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// proj
import {
    ChartPage
} from 'pages';
import { book } from './index.js';

export default class Public extends Component {
    render() {
        return (
            <Switch>
                <Route exact component={ ChartPage } path={ book.charts } />
                <Redirect to={ book.charts } />
            </Switch>
        );
    }
}
