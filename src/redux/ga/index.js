// vendor
import { delay } from 'redux-saga';
import { call, all, cancel, fork, put, select, take } from 'redux-saga/effects';
import { v4 } from 'uuid';

/**
 * Constants
 * */
export const moduleName = 'ga';
const prefix = `ga/${moduleName}`;

const SET_TIMEOUT_INTERVAL = `${prefix}/SET_TIMEOUT_INTERVAL`;
const SET_EVALUATION = `${prefix}/SET_EVALUATION`;
const SET_DATA = `${prefix}/SET_DATA`;
const SET_INDEX = `${prefix}/SET_INDEX`;
const SET_IS_RUNNING = `${prefix}/SET_IS_RUNNING`;
const SET_DATA_ID = `${prefix}/SET_DATA_ID`;

const START_BACKGROUND_SYNC = `${prefix}/START_BACKGROUND_SYNC`;
const STOP_BACKGROUND_SYNC = `${prefix}/STOP_BACKGROUND_SYNC`;

/**
 * Reducer
 * */

const ReducerState = {
    isRunning:       false,
    evaluation:      'deba1',
    index:           0,
    timeoutInterval: 500,
    dataHistory:     {},
};

export default function reducer(state = ReducerState, action) {
    const { type, payload } = action;
    const dataId = v4();

    switch (type) {
        case SET_DATA:
            return {
                ...state,
                index:       0,
                dataHistory: {
                    ...state.dataHistory,
                    [ dataId ]: {
                        name: payload.name,
                        data: payload.data,
                        id:   dataId,
                    },
                },
                dataId,
            };
        case SET_EVALUATION:
            return {
                ...state,
                evaluation: payload,
                index:      0,
            };
        case SET_DATA_ID:
            return {
                ...state,
                dataId:    payload,
                isRunning: false,
                index:     0,
            };
        case SET_INDEX:
            return {
                ...state,
                index: payload,
            };
        case SET_IS_RUNNING:
            return {
                ...state,
                isRunning: payload,
            };
        case SET_TIMEOUT_INTERVAL:
            return {
                ...state,
                timeoutInterval: payload,
            };
        default:
            return state;
    }
}

/**
 * Actions
 * */

export const setTimeoutInterval = value => ({
    type:    SET_TIMEOUT_INTERVAL,
    payload: value,
});

export const setEvaluation = func => ({
    type:    SET_EVALUATION,
    payload: func,
});

export const setData = (data, name) => ({
    type:    SET_DATA,
    payload: { data, name },
});

export const setIndex = index => ({
    type:    SET_INDEX,
    payload: index,
});

export const startBackgroundSync = () => ({
    type: START_BACKGROUND_SYNC,
});

export const stopBackgroundSync = () => ({
    type: STOP_BACKGROUND_SYNC,
});

export const setIsRunning = isRunning => ({
    type:    SET_IS_RUNNING,
    payload: isRunning,
});

export const setDataId = dataId => ({
    type:    SET_DATA_ID,
    payload: dataId,
});

/**
 * Selectors
 */

const getIterationState = state => ({
    index:     state.ga.index,
    isRunning: state.ga.isRunning,
    data:      state.ga.dataId ? state.ga.dataHistory[ state.ga.dataId ].data : void 0,
    dataId:    state.ga.dataId,
});

export const getIterationData = state => ({
    evaluation:      state.ga.evaluation,
    timeoutInterval: state.ga.timeoutInterval,
    dataHistory:     state.ga.dataHistory,
    ...getIterationState(state),
});

const getTimeoutInterval = state => ({
    timeoutInterval: state.ga.timeoutInterval,
});

/**
 * Saga
 **/

function* iterationSaga() {
    while (true) {
        const { timeoutInterval } = yield select(getTimeoutInterval);

        yield delay(timeoutInterval);

        const { isRunning, index, data } = yield select(getIterationState);

        if (isRunning && index < data.length - 1) {
            yield put(setIndex(index + 1));
        }

        if (data && index === data.length - 1) {
            yield put(setIsRunning(false));
        }
    }
}

function* iterationControllerSaga() {
    while (yield take(START_BACKGROUND_SYNC)) {
        const bgSyncTask = yield fork(iterationSaga);

        yield take(STOP_BACKGROUND_SYNC);
        yield cancel(bgSyncTask);
    }
}

export function* saga() {
    yield all([ call(iterationControllerSaga) ]);
}
