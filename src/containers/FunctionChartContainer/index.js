/* eslint-disable no-sync */
// vendor
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';

// proj
import { FunctionChart, ControlPanel, HistoryList } from '../../components';
import {
    setData,
    setEvaluation,
    setIndex,
    setTimeoutInterval,
    setIsRunning,
    startBackgroundSync,
    stopBackgroundSync,
    setDataId,
    getIterationData,
} from './../../redux/ga';

// own
// import Styles from './styles.module.css';

class FunctionChartContainer extends Component {
    componentWillUnmount() {
        this.props.stopBackgroundSync();
    }

    componentDidMount() {
        this.props.startBackgroundSync();
    }

    render() {
        const {
            index,
            data,
            evaluation,
            isRunning,
            timeoutInterval,
            dataId,
            dataHistory,
        } = this.props;

        return (
            <>
                {data ? (
                    <div
                        style={ {
                            display:        'flex',
                            justifyContent: 'center',
                            fontSize:       18,
                        } }
                    >
                        <b>
                            Iteration: { index + 1 } out of { data.length }
                        </b>
                    </div>
                ) :
                    void 0
                }
                <Row>
                    <Col span={ 6 } style={ { padding: 20 } }>
                        <HistoryList
                            dataId={ dataId }
                            dataHistory={ dataHistory }
                            setDataId={ this.props.setDataId }
                        />
                    </Col>
                    <Col span={ 18 } style={ { padding: 20 } }>
                        <ControlPanel
                            index={ index }
                            data={ data }
                            evaluation={ evaluation }
                            isRunning={ isRunning }
                            timeoutInterval={ timeoutInterval }
                            setTimeoutInterval={ this.props.setTimeoutInterval }
                            setData={ this.props.setData }
                            setEvaluation={ this.props.setEvaluation }
                            setIndex={ this.props.setIndex }
                            setIsRunning={ this.props.setIsRunning }
                        />
                    </Col>
                </Row>

                <FunctionChart
                    index={ index }
                    data={ data }
                    evaluation={ evaluation }
                />
            </>
        );
    }
}

const mapStateToProps = state => ({ ...getIterationData(state) });
const mapDispatchToProps = {
    setData,
    setDataId,
    setEvaluation,
    setIndex,
    setTimeoutInterval,
    setIsRunning,
    startBackgroundSync,
    stopBackgroundSync,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FunctionChartContainer);
