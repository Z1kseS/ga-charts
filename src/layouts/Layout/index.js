// vendor
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Menu } from 'antd';

// own
// import Styles from './styles.module.css';

const { Content, Footer, Header } = Layout;

class LayoutComponent extends Component {
    render() {
        return (
            <Layout style={ { minHeight: '100vh', backgroundColor: '#fff' } }>
                <Header style={ { position: 'fixed', zIndex: 1, width: '100%' } }>
                    <div className='logo' />
                    <Menu
                        theme='dark'
                        mode='horizontal'
                        defaultSelectedKeys={ [ '1' ] }
                        style={ { lineHeight: '64px' } }
                    >
                        <Menu.Item key='1'>Графік</Menu.Item>
                    </Menu>
                </Header>
                <Content
                    style={ {
                        padding:   '20px 50px',
                        marginTop: 64,
                    } }
                >
                    { this.props.children }
                </Content>
                <Footer style={ { textAlign: 'center' } }>
                    Ant Design ©2018 Created by Ant UED
                </Footer>
            </Layout>
        );
    }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export const ConnectedLayout = connect(
    mapStateToProps,
    mapDispatchToProps,
)(LayoutComponent);
