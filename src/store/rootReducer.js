// vendor
import { combineReducers } from 'redux';

// proj
import gaReducer, { moduleName as gaModule } from '../redux/ga';

const appState = {
    [ gaModule ]: gaReducer,
};

const rootReducer = combineReducers({ ...appState });

export default rootReducer;
