// vendor
import { all } from 'redux-saga/effects';

// proj
import { saga as gaSaga } from '../redux/ga';

export default function* rootSaga() {
    yield all([ gaSaga() ]);
}
