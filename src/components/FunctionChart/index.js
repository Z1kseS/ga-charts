import React, { Component } from 'react';
import Chart from 'chart.js';
import { Line } from 'react-chartjs-2';

import { functions } from 'utils';

Chart.pluginService.register({
    beforeInit: chart => {
        // We get the chart data
        const data = chart.config.data;

        // For every dataset ...
        for (
            let datasetIndex = 0;
            datasetIndex < data.datasets.length;
            datasetIndex++
        ) {
            // For every label ...
            for (
                let labelIndex = 0;
                labelIndex < data.labels.length;
                labelIndex++
            ) {
                // We get the dataset's function and calculate the value
                let fct = data.datasets[ datasetIndex ].function,
                    x = data.labels[ labelIndex ],
                    y = fct(x);
                // Then we add the value to the dataset data
                data.datasets[ datasetIndex ].data.push(y);
            }
        }
    },
});

const options = {
    responsive:          true,
    maintainAspectRatio: false,
    animation:           {
        duration: 0, // general animation time
    },
    scales: {
        yAxes: [
            {
                ticks: {
                    // beginAtZero: true,
                },
            },
        ],
    },
};

const getData = (testData, index, evaluation) => {
    const { min, max, evaluate, precision } = functions[ evaluation ];

    return {
        labels: new Array(Math.round((max - min) * precision))
            .fill(min)
            .map((value, index) => Number(index) / precision + min)
            .map(num => num.toFixed(Math.log10(precision))),
        datasets: [
            {
                label:    'seeds',
                function: x => {
                    const has = testData[ index ].find(
                        x0 =>
                            Math.round(x0.decodedParams[ 0 ] * precision) ===
                            Math.round(x * precision),
                    );
                    const value = has ? evaluate(x) : void 0;

                    return value;
                },
                data:             [], // Don't forget to add an empty data array, or else it will break
                borderColor:      'red',
                showLine:         false,
                pointBorderWidth: 5,
                pointRadius:      2,
            },
            {
                label:       'deba1',
                function:    evaluate,
                data:        [],
                borderColor: 'rgba(75, 192, 192, 1)',
                fill:        false,
                pointRadius: 0,
            },
        ],
    };
};

export class FunctionChart extends Component {
    render() {
        const { data, index, evaluation } = this.props;

        return (
            <div
                style={ {
                    display:        'flex',
                    justifyContent: 'center',
                    alignItems:     'center',
                } }
            >
                <div
                    id='chart'
                    style={ {
                        width:          '70%',
                        display:        'flex',
                        justifyContent: 'center',
                        alignItems:     'center',
                    } }
                >
                    { data && (
                        <Line
                            redraw
                            height={ 500 }
                            data={ getData(data, index, evaluation) }
                            options={ options }
                        />
                    ) }
                </div>
            </div>
        );
    }
}
