import React, { Component } from 'react';
import { List } from 'antd';
import _ from 'lodash';

export class HistoryList extends Component {
    render() {
        const { dataHistory, setDataId, dataId } = this.props;

        return (
            <List
                size='small'
                header={ <div>Дані</div> }
                bordered
                dataSource={ _.values(dataHistory) }
                renderItem={ item => (
                    <List.Item
                        style={ { cursor: 'pointer' } }
                        onClick={ () => setDataId(item.id) }
                    >
                        { dataId === item.id ? '* ' : '' }
                        { item.name }
                    </List.Item>
                ) }
            />
        );
    }
}
