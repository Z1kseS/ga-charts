import React, { Component } from 'react';
import _ from 'lodash';
import {
    Button,
    Input,
    Select,
    Upload,
    Icon,
    Row,
    Col,
    InputNumber,
} from 'antd';

import { functions } from 'utils';

const Dragger = Upload.Dragger;

export class ControlPanel extends Component {
    handleFileUpload(file) {
        const { setData } = this.props;
        const fileReader = new FileReader();

        fileReader.onloadend = () =>
            setData(JSON.parse(fileReader.result), file.name);
        fileReader.readAsText(file);
    }

    canRun() {
        const { isRunning, data, evaluation } = this.props;

        return !isRunning && data && evaluation;
    }

    render() {
        const {
            evaluation,
            timeoutInterval,
            isRunning,
            data,
            index,
        } = this.props;

        const {
            setEvaluation,
            setIsRunning,
            setTimeoutInterval,
            setIndex,
        } = this.props;

        return (
            <Col>
                <Row>
                    <Dragger
                        id='fileInput'
                        name={ 'file' }
                        showUploadList={ false }
                        beforeUpload={ () => false }
                        onChange={ info => {
                            this.handleFileUpload(info.file);
                        } }
                    >
                        <p className='ant-upload-drag-icon'>
                            <Icon type='inbox' />
                        </p>
                        <p className='ant-upload-text'>
                            Виберіть логи роботи генетичного алгоритму
                        </p>
                    </Dragger>
                </Row>
                <Row>
                    Швидкість{ ' ' }
                    <Input
                        type='number'
                        min={ 0 }
                        value={ timeoutInterval }
                        onChange={ e => {
                            if (e.target.value && e.target.value > 0) {
                                setTimeoutInterval(e.target.value);
                            } else {
                                setTimeoutInterval(0);
                            }
                        } }
                    />
                </Row>
                <Row>
                    Функція
                    <Select
                        id='evaluationSelect'
                        style={ { width: '100%' } }
                        value={ evaluation }
                        onSelect={ option => setEvaluation(option) }
                    >
                        { _.keys(functions).map(name => {
                            return (
                                <Select.Option
                                    id={ name }
                                    key={ name }
                                    value={ name }
                                >
                                    { name }
                                </Select.Option>
                            );
                        }) }
                    </Select>
                </Row>
                <Row>
                    Індекс
                    <InputNumber
                        disabled={ !data }
                        min={ 1 }
                        style={ { width: '100%' } }
                        max={ (data || []).length }
                        value={ index + 1 }
                        onChange={ value => {
                            if (
                                value &&
                                value <= (data || []).length &&
                                value > 1
                            ) {
                                setIndex(value - 1);
                            }
                        } }
                    />
                </Row>
                <br />
                <Row type='flex' gutter={ 16 } justify='center'>
                    <Col>
                        <Button
                            disabled={ !this.canRun() }
                            onClick={ () => setIsRunning(true) }
                        >
                            Почати
                        </Button>
                    </Col>
                    <Col>
                        <Button
                            disabled={ !isRunning }
                            onClick={ () => setIsRunning(false) }
                        >
                            Зупинити
                        </Button>
                    </Col>
                </Row>
            </Col>
        );
    }
}
