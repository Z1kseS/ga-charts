// vendor
import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

// proj
import { Layout } from '../../layouts';
import { FunctionChartContainer } from '../../containers';

export class ChartPage extends Component {
    render() {
        return (
            <DocumentTitle title='GA Example'>
                <Layout>
                    <FunctionChartContainer />
                </Layout>
            </DocumentTitle>
        );
    }
}
